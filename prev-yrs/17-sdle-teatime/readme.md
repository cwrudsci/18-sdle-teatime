# This is the README for the 2017 CWRU SDLE Tea-time Learnings Git Repository

## SDLE Tea-times: 3:00 to 3:45, on Tuesdays, Wednesdays and Thursdays in Bingham 204.
## Starting Tuesday June 13th, through Thursday July 20th, 2017
## A set of teachings of Data Science topics such as Git, Linux, R, Python, HPC, Hadoop, HBase, and Spark.
### Any interested people are invited. 
### These are intended to be simple startups in these topics.
### These are relatively informal, but structured. 

## Here we will share datasets, R codes and markdown reports. 
### SDLE Tea-time Videos are on [Periscope (Live)] [9] and [YouTube] [10]
## This is the second year of Tea-time, for files from previous years, look in the subfolder 'prev-yrs'.

## Project Title: SDLE Tea-time Learnings: Data Science (R, Python, Git, Linux, HPC, Hadoop, HBase, Spark)
 
## Authors: N. Wheeler, A. Karimi, R. H. French, in collaboration with the students of the Case Western Reserve University SDLE Research Center 

[Case Western Reserve University, SDLElab] [1]
 
### [SDLE REDCap Sample Database Login] [3]
### [The R Project for Statistical Computing] [4]
### [RStudio Integragrated Development Enviroment (IDE) for R] [5]
### [RMarkdown for open science collaboration & reporting] [6]
### [Frenchrh @frenchrh on Twitter] [7]
### [SDLE_ResCntr @SDLE_ResCntr on Twitter] [8]


[1]: http://sdle.case.edu
[2]: 
[3]: https://dcru.case.edu/redcap/
[4]: http://www.r-project.org/
[5]: http://www.rstudio.com/products/RStudio/
[6]: http://rmarkdown.rstudio.com/
[7]: https://twitter.com/frenchrh
[8]: https://twitter.com/SDLE_ResCntr
[9]: https://www.periscope.tv/SDLE_ResCntr/1RDxlwgLavgJL
[10]: https://www.youtube.com/playlist?list=PLBrrkqzxVsYjiVi9W-3j_bcxgWNq7RqXG
