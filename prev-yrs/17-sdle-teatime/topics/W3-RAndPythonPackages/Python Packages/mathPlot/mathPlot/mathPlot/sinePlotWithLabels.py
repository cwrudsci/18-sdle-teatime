def sinePlotWithLabels():
    """This function plots a sine curve.
    """
    from mathPlot.sinePlot import sinePlot
    import matplotlib.pyplot as plt
    sinePlot()
    plt.xlabel('x',fontsize = 15)
    plt.ylabel('y',fontsize = 15)
        
