def sinePlot():
    """This function plots a sine curve.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    x = np.linspace(1,10,1e6)
    y = np.sin(x)
    plt.plot(x,y)
