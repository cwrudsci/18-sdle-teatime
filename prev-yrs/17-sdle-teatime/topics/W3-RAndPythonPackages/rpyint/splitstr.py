# Python Script to call from R
# splitstr.py
import sys
import pandas

# Get the arguments passed in
string = sys.argv[1]
pattern = sys.argv[2]

# Perform the splitting
ans = string.split(pattern)

# Write the results to a csv via pandas
s = pandas.Series(ans)
s.to_csv("py-to-r.csv")

# Join the resulting list of elements into a single
# comma delimited string and print to CLI
print(','.join(ans))
