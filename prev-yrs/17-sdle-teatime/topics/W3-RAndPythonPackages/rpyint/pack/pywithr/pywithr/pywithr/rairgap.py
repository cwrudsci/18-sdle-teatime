def rairgap(a1,a2,a3):
    """This function adds two numbers together.

    Args:
        arg1 (numeric): The first parameter.
        arg2 (numeric): The second parameter.

    Returns:
        res (numeric): The result of adding the two input parameters together.
    """
    import subprocess
    import os
    import pandas
    this_dir, this_filename = os.path.split(__file__)
    path2script = os.path.join(this_dir, "files/data/rag.R")
    args = [str(a1),str(a2),str(a3)]
    cmd = ["Rscript", path2script] + args
    os.system(' '.join(cmd))
    res = pandas.read_csv('r-to-py.csv')
    os.remove('r-to-py.csv')
    return(res)
