#' minus
#'
#' Function description.
#'
#' @name minus
#' @param x The first parameter.
#' @param y The second parameter.
#' @return What is the returned value, if any.
#' @export
#' @examples
#' minus(2,1)
#' minus(1,0)
minus <- function(x,y){
  return(x-y)
}
