# -*- coding: utf-8 -*-
"""
Created on Mon Oct 03 14:48:12 2016

@author: dxb507
"""

import types

def display_param(x):

    if isinstance(x, types.DictType):
        for key in x:
            print ("Key = %s, Value = %g" % (key,x[key]))
    else:
        try:
            for i in x:
                print(i)
        except TypeError:
            print(x)
