# -*- coding: utf-8 -*-

# This file creates a random 2D array for playing purposes

import numpy as np

np.random.seed(2)

my_arr = np.random.normal(size=(5,5))

np.savetxt('mycsvarray.csv', my_arr, fmt='%.16g', delimiter=',')