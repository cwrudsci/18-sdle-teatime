# -*- coding: utf-8 -*-

import numpy as np


# Generate a 1D array with integer numbers from 0 to 9

my_arr = np.arange(10)

# Save the array into the file my_array.npy

np.save('my_array',my_arr);

# Load the array from the file into the my_arr_saved

my_arr_saved = np.load('my_array.npy')

# Save the array in an npz (ZIP) format

np.savez('my_zipped_arrays.npz',a=my_arr,b=my_arr)

# Load the (ZIP) file into the my_zipped_array

zipped_arr = np.load('my_zipped_arrays.npz')
