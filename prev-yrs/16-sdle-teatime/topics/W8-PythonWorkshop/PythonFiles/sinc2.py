# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt

points = np.linspace(-5,5,1000)

x, y = np.meshgrid(points,points)

z = np.sinc( np.sqrt( x**2 + y**2))

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.imshow(z, extent=[-5,5,-5,5], cmap='gnuplot')
ax.set_title("Plot of $\sin(\sqrt{x^2+y^2})/\sqrt{x^2+y^2}$")