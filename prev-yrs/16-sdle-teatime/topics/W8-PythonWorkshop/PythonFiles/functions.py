# -*- coding: utf-8 -*-

def add_two(x):
    """adds two to the given number x"""
    return x+2
    
# Functions are objects, and as so, we can pass them to functions as 
# arguments!

def apply_to_three(f):
    """this function takes a function as an argument
    and it plugs the number three into it"""
    return f(3)
