# -*- coding: utf-8 -*-

my_grades = { "Python101" : 100, "others" : 99 }

for key in my_grades:
    print("Key = %s, Grade = %d" % (key, my_grades[key]))
