temp1 <- read.csv("/home/wxh272/Downloads/teatime-ex1.csv")
temp2 <- read.csv("/home/wxh272/Downloads/teatime-ex2.csv")


## create the personal data frame
## make sure the first column is the rowkey
t1 <- c(rowkey= 'rk1','rk2','rk3')
t2 <- c(name= 'WeiHeng','John','Mary')
t3 <- c(city='Tainan','Cleveland','NewYork')
t4 <- c(gend= 'Male','Male','Female')
temp1 <- data.frame(rowkey=t1,name=t2,city=t3,gender=t4)

## use crad_put to ingest data into Hbase
## crad_put(table_name, family_name, df)
rcradletools::crad_put('teatime','personal',temp1)


## create the detail data frame
## make sure the frist column is the rowkey
t1 <- c(rowkey= 'rk1','rk2','rk3')
d2 <- c(university = 'NCTU', 'CWRU', 'NYU')
d3 <- c(department = 'Stat', 'MSE', 'CS')
temp2 <- data.frame(rowkey=t1,university=d2,department=d3)

rcradletools::crad_put('teatime','detail',temp2)


## retrieve the data from Hbase
df_t1 <- rcradletools::crad_get('teatime','rk1')
df_t2 <- rcradletools::crad_get('teatime','rk2')
df_t3 <- rcradletools::crad_get('teatime','rk3')

df <- rbind(df_t1,df_t2,df_t3)

## filter the data on certain criteria in Hbase table
df_t11 <- rcradletools::crad_fltr('teatime','personal','gender','Male')

df_t22 <- rcradletools::crad_fltr('teatime','detail','university','NCTU')


