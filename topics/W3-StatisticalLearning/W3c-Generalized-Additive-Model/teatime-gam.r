## Load the package from Introduction to Statistical Learning
library(ISLR)

## load the Wage data
attach(Wage)

## View data
View(Wage)

##-------- fit a polynomial regression with 4 df ----------
fit <- lm(wage~poly(age,4),data=Wage)
summary(fit)

fit.1 <- lm(wage~age+I(age^2)+I(age^3)+I(age^4), data=Wage)
summary(fit.1)

## create a grid of value for age for predictions
agelims <- range(age)
age.grid <- seq(agelims[1],agelims[2])

newdf <- data.frame(age = age.grid)
## predicted values
pred <- predict(fit,newdata=newdf,se=TRUE)

## plot the fitted model with 95% confidence interval
## superimposed the data
plot(age ,wage ,col =" gray ")
lines(age.grid ,pred$fit ,lwd =2)
lines(age.grid ,pred$fit+2*pred$se ,lty ="dashed")
lines(age.grid ,pred$fit-2*pred$se ,lty ="dashed")


##---- fit a generalized additive model with spline ---------------

## load spline library
library(splines)

##------- Use the B-spline ---------------

## specific knots
fit1 <- lm(wage~bs(age,knots=c(25,40,60)),data=Wage)
summary(fit1)

## predicted values
pred1 <- predict(fit1,newdata =newdf,se=T)

## plot the fitted model with 95% confidence interval
plot(age ,wage ,col =" gray ")
lines(age.grid ,pred1$fit ,lwd =2)
lines(age.grid ,pred1$fit+2*pred1$se ,lty ="dashed")
lines(age.grid ,pred1$fit-2*pred1$se ,lty ="dashed")


## df: degree of freedom, default is Null
## choose (df-degree) knots at quantile of x (equal space)
fit1.1 <- lm(wage~bs(age,df=6),data=Wage)
summary(fit1.1)
coef(fit1.1)

## same as using knots at equal space
var <- c(25,50,75)
knot.q <- quantile(age,var/100)
fit1.11 <- lm(wage~bs(age,knots=knot.q),data=Wage)
summary(fit1.11)
coef(fit1.11)


## degree: degree of the piecewise polynomial
## defaul is 3
fit1.2 <- lm(wage~bs(age,df=6,degree=2),data=Wage)
summary(fit1.2)
coef(fit1.2)

## same as using knots at equal space
var <- c(20,40,60,80)
knot.q <- quantile(age,var/100)
fit1.22 <- lm(wage~bs(age,knots=knot.q,degree=2),data=Wage)
summary(fit1.22)
coef(fit1.22)


##------- Use the natural spline ---------------

## specific knots
fit2 <- lm(wage~ns(age,knots=c(25,40,60)),data=Wage)
summary(fit2)

fit21 <- lm(wage~ns(age,knots=c(25)),data=Wage)
summary(fit21)


## predicted values
pred2 <- predict(fit2,newdata =list(age =age.grid),se=T)

## plot the fitted model with 95% confidence interval
plot(age ,wage ,col =" gray ")
lines(age.grid ,pred2$fit ,lwd =2)
lines(age.grid ,pred2$fit+2*pred2$se ,lty ="dashed")
lines(age.grid ,pred2$fit-2*pred2$se ,lty ="dashed")

## plot the b-spline and natural spline together
plot(age ,wage ,col =" gray ")
lines(age.grid ,pred1$fit ,lwd =2,col='red')
lines(age.grid ,pred1$fit+2*pred1$se ,lty ="dashed",col='red')
lines(age.grid ,pred1$fit-2*pred1$se ,lty ="dashed",col='red')
lines(age.grid ,pred2$fit ,lwd =2,col='blue')
lines(age.grid ,pred2$fit+2*pred2$se ,lty ="dashed",col='blue')
lines(age.grid ,pred2$fit-2*pred2$se ,lty ="dashed",col='blue')
legend(55,310,legend =c("Cubic Spline" ,"Natural Cubic Spline"),
       col=c("red",'blue'), lwd =c(1,2), cex =.8)


## df: degree of freedom, default is 1
## choose (df-1-intercept) knots at quantile of x (equal space)
fit2.1 <- lm(wage~ns(age,df=4),data=Wage)
summary(fit2.1)
coef(fit2.1)

## same as using knots at equal space
var <- c(25,50,75)
knot.q <- quantile(age,var/100)
fit2.11 <- lm(wage~ns(age,knots=knot.q),data=Wage)
summary(fit2.11)
coef(fit2.11)


##------- smoothing spline ----------
fit3 <- smooth.spline(age ,wage ,df =16)
fit3.1 <- smooth.spline(age ,wage ,cv=TRUE)
fit3.1$df

plot(age ,wage ,xlim=agelims ,cex =.5, col =" darkgrey ")
title (" Smoothing Spline ")
lines(fit3 ,col ="red ",lwd =2)
lines(fit3.1 ,col ="blue ",lwd =2)
legend(65,310,legend =c("16 DF " ,"6.8 DF"),
       col=c("red "," blue "),lty =1, lwd =2, cex =.8)




##------- fit a GAM with two preditors
fit4 <- lm(wage~bs(age,df=5)+ns(year,df=4),data=Wage)
summary(fit4)

library(gam)
par(mfrow=c(1,2))
plot.Gam(fit4,se=T,col="red")


## use 'gam' package
library(gam)
gam.m1 <- gam(wage ~ s(age,5)+s(year,4),data=Wage)
summary(gam.m1)

## plot fitted model
par(mfrow=c(1,2))
plot.Gam(gam.m1,se=T,col="red")


gam.m2 <- gam(wage ~ s(age,5)+s(year,1),data=Wage)
summary(gam.m2)


## use 'mgcv' package
## Mixed GAM Computation Vehicle with automatic smoothness estimation
library(mgcv) 
gam.m3 <- gam(wage~s(age,k=5)+s(year,k=4),data=Wage)
summary(gam.m3)

par(mfrow=c(1,2))
plot(gam.m3)

