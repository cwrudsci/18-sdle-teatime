# -*- coding: utf-8 -*-
"""
Created on Fri Jul 13 15:34:38 2018

@author: jiqiliu
"""
import sys
sys.path.append("/home/rxf131/pythonpackages/lib/python2.7.13/site-packages")
from PIL import Image
import scipy
import os
import scipy.misc as mi
import cv2
import numpy as np
from matplotlib import pyplot as plt
import scipy.ndimage
from skimage import filters
from skimage.filters.thresholding import threshold_otsu

#figure directory
dir = '/home/jxl1763/Downloads/figure/'
#dir = '/home/jiqiliu/Documents/MLEET/img/figure/'
file = 'face.png'
path = os.path.join(dir,file)
#a = sp.misc.imread(path, mode = 'RGB')
#plt.imshow(a[:,:,2]) 
#read image into environment
a = Image.open(path).convert('L')
a
#transfer image to an array
b = np.asarray(a)
#make the histogram of pixel
hist = cv2.calcHist([b],[0],None,[256],[0,256])
plt.hist(b.ravel(),50,[0,256])
plt.title('Histogram for gray scale Smile Face')
plt.show()

#test the spacial filter theory
b[5,5]
#testing
#Mean filter
k = np.ones((3,3))/9
c = scipy.ndimage.filters.convolve(a, k,mode='reflect',cval=0.0)
c[5,5]
c = scipy.ndimage.filters.median_filter(a,size=3,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
c[5,5]
c = scipy.ndimage.filters.maximum_filter(a,size=3,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
c[5,5]
c = scipy.ndimage.filters.minimum_filter(a,size=3,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
c[5,5]

#show the spacial filter effect
file = 'straw.png'
path = os.path.join(dir,file)
a = Image.open(path).convert('L')
b = np.asarray(a)
a
#mean
k = np.ones((5,5))/25
c = scipy.ndimage.filters.convolve(a, k,mode='reflect',cval=0.0)
Image.fromarray(c)
#scipy.misc.toimage(c)
#median
c = scipy.ndimage.filters.median_filter(a,size=5,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
Image.fromarray(c)
#scipy.misc.toimage(c)
#max
c = scipy.ndimage.filters.maximum_filter(a,size=5,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
Image.fromarray(c)
#scipy.misc.toimage(c)
#min
c = scipy.ndimage.filters.minimum_filter(a,size=5,footprint=None,output=None,mode='reflect',cval=0.0,origin=0)
Image.fromarray(c)
#scipy.misc.toimage(c)


#edge detection
file = 'qmnx.jpeg'
path = os.path.join(dir,file)
a = Image.open(path).convert('L')
a
b = np.asarray(a)
#b = mi.fromimage(a)
#scipy.misc.toimage(b)
Image.fromarray(b)

b = filters.sobel(a)
#scipy.misc.toimage(b)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

b = filters.sobel_v(a)
#scipy.misc.toimage(b)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

b = filters.sobel_h(a)
#scipy.misc.toimage(b)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

b = filters.prewitt(a)
#scipy.misc.toimage(b)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

b = filters.prewitt_v(a)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

#scipy.misc.toimage(b)
b = filters.prewitt_h(a)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)

#scipy.misc.toimage(b)
b = scipy.ndimage.filters.laplace(a,mode='reflect',cval = 1.0)
#scipy.misc.toimage(b)
Image.fromarray(b)

b = scipy.ndimage.filters.gaussian_laplace(a,1,mode='reflect')
#scipy.misc.toimage(b)
Image.fromarray(c)
#edge detection


#enhancement

#inverse
file = 'qmnx.jpeg'
path = os.path.join(dir,file)
a = Image.open(path).convert('L')
a
b = np.asarray(a)
#b = mi.fromimage(a)
b = 255-b
#scipy.misc.toimage(b)
Image.fromarray(b)

#strenching
#b = mi.fromimage(a)
b = np.asarray(a)
c = b.astype(float)
b = 255 * (c - b.max()) / (b.max() - b.min())
#scipy.misc.toimage(b)
c = 255 * c/ c.max()
c = c.astype(np.uint8)
Image.fromarray(c)
#thresholding
file = 'cell.jpg'
path = os.path.join(dir,file)
a = Image.open(path).convert('L')
a
b = np.asarray(a)
#b = mi.fromimage(a)
hist = cv2.calcHist([b],[0],None,[256],[0,256])
plt.hist(b.ravel(),50,[0,256])
plt.title('Histogram for gray scale QMNX')
plt.show()
#otsu get threshold value
thresh = threshold_otsu(b)
thresh
b = b > thresh
b = b.astype(int)
b = 255 * b/ b.max()
c = b.astype(np.uint8)
Image.fromarray(c)
#scipy.misc.toimage(b)

#different operation of threshold
file = 'cell.jpg'
path = os.path.join(dir,file)
a = Image.open(path).convert('L')
a
b = mi.fromimage(a)
ret,thresh1 = cv2.threshold(b,thresh,255,cv2.THRESH_BINARY)
ret,thresh2 = cv2.threshold(b,thresh,255,cv2.THRESH_BINARY_INV)
ret,thresh3 = cv2.threshold(b,thresh,255,cv2.THRESH_TRUNC)
ret,thresh4 = cv2.threshold(b,thresh,255,cv2.THRESH_TOZERO)
ret,thresh5 = cv2.threshold(b,thresh,255,cv2.THRESH_TOZERO_INV)

titles = ['Original Image','BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
images = [b, thresh1, thresh2, thresh3, thresh4, thresh5]

for i in xrange(6):
    plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

plt.show()


